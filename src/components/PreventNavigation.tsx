import React, { useState } from 'react';
import { Prompt } from 'react-router-dom';
import { Typography, TextField, Button } from '@material-ui/core';

const PreventNavigation: React.FC = () => {
  let [isBlocking, setIsBlocking] = useState(false);
  let [txtFieldValue, setTextFieldValue] = useState<String>();

  const onSubmit = (event: React.MouseEvent): void  => {
    event.preventDefault();
    setTextFieldValue("");
    setIsBlocking(false);
  }

  return (
    <div>
      <Prompt
        when={isBlocking}
        message={location =>
          `Are you sure you want to go to ${location.pathname}`
        }
      />
      <Typography>
        Blocking?{" "}
        {isBlocking ? "Yes, click submit to clear txt field" : "Not blocking"}
      </Typography>
      <TextField id="standard-basic" 
        label="Standard"
        fullWidth
        placeholder="Please input something to try"
        value = {txtFieldValue}
        onChange={event => {
          setIsBlocking(event.target.value.length > 0);
          setTextFieldValue(event.target.value);
        }}
        />
      <Button variant="contained" color='primary' onClick = {event => onSubmit(event)}>
        Click to Submit
      </Button>
    </div>
  );
}

export default PreventNavigation;