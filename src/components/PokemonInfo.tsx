import React, { useEffect, useState, useCallback } from 'react';
import { useParams } from 'react-router'
import { makeStyles, Typography, Paper } from '@material-ui/core';

interface EachPokemon {
  name: string;
  frontImgUrl: string;
  backImgUrl: string;
}

const useStyle = makeStyles(theme => ({
  title: {
    textAlign: 'center'
  },
  imageContainer: {
    display: 'flex',
    flexGrow: 1,
    justifyContent: 'space-around',
  },
  imagePaper: {
    padding: theme.spacing(5),
    textAlign: 'center'
  },
}));

const PokemonInfo: React.FC = (props) => {
  const { id } = useParams();
  const classes = useStyle();
  // const match = useRouteMatch();
  const [pokemon, setPokemon] = useState<EachPokemon>({
    name: 'Loading',
    frontImgUrl: '',
    backImgUrl: '',
  });

  const getPokemonInfo = useCallback(async () => {
    const data = await fetch(`https://pokeapi.co/api/v2/pokemon/${id}`);
    const pokemonJson = await data.json();
    // console.log(pokemonJson);
    setPokemon({
      name: pokemonJson.name,
      frontImgUrl: pokemonJson.sprites.front_default,
      backImgUrl: pokemonJson.sprites.back_default,
    });
  },[id]);

  useEffect(() => {
    getPokemonInfo();
    // console.log("props", props);
    // console.log("match", match);
  },[getPokemonInfo])

  return (
    <div>
      {console.log(pokemon)}
      <h1 className={classes.title}>{pokemon.name.toLocaleUpperCase()}</h1>
      <div className={classes.imageContainer}>
        <Paper className={classes.imagePaper}>
          <Typography>Front Image of {pokemon.name}</Typography>
          <img src={pokemon.frontImgUrl} alt={`front-img of ${pokemon.name}`}/>
        </Paper>
        <Paper className={classes.imagePaper}>
          <Typography>Back Image of {pokemon.name}</Typography>
          <img src={pokemon.backImgUrl} alt={`back-img of ${pokemon.name}`}/>
        </Paper>
      </div>
    </div>
  );
}

export default PokemonInfo;