import React, { useEffect, useState, useCallback } from 'react';
import { makeStyles, Paper, Button } from '@material-ui/core';
import { Link } from 'react-router-dom';

interface PokemonsInfo {
  name: string;
  url: string;
}

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  pokemonContainer: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexWrap: 'wrap'
  },
  pokemonCard: {
    padding: theme.spacing(5),
    margin: theme.spacing(2),
    width: '300px',
    textAlign: 'center'
  },
  button: {
    width: '50%',
    height: '3rem',
    alignSelf: 'center',
    marginTop: theme.spacing(4)
  },
  link: {
    color: 'black',
    textDecoration: 'none'
  }
}));

const Pokemons: React.FC = (props) => {
  const classes = useStyles(props);
  const [pokemonList, setPokemons] = useState<PokemonsInfo[]>([]);
  const [pagination, setPagination] = useState<string>('');

  const fetchCallBack = useCallback( async (url: string) => {
    const data = await fetch(url);
    const pokemonJson = await data.json();
    console.log(pokemonJson);
    setPokemons(oldList => [...oldList, ...pokemonJson.results]);
    setPagination(pokemonJson.next);
  },[])

  useEffect(() => {
    fetchCallBack('https://pokeapi.co/api/v2/pokemon');
  }, [fetchCallBack]);

  return (
    <div className={classes.root}>
      {console.log(pagination, pokemonList)}
      <div className={classes.pokemonContainer}>
        {pokemonList.map((pokemon,index) => {
          return <Link to={`/pokemons/${index+1}`} key={index} className={classes.link}>
              <Paper className={classes.pokemonCard}>
                {pokemon.name}
              </Paper>
            </Link>
        })}
      </div>
      <Button color='primary' variant="contained" className={classes.button} onClick={() => fetchCallBack(pagination)}>Fetch More!</Button>
    </div>
  )
}

export default Pokemons;