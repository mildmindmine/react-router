import React from 'react';
import {Typography} from '@material-ui/core';
import { useLocation } from 'react-router-dom';

const NotFound: React.FC = (prop) => {
  let location = useLocation();

  return <div>
    <Typography>
      Sorry Cannot Find the Requested Path "{location.pathname}"
    </Typography>
  </div>
}

export default NotFound;