import React from 'react';
import { Route, Redirect } from 'react-router-dom';

interface PrivateRouteProps {
  isLogin: boolean;
  path: string;
}

const PrivateRoute: React.FC<PrivateRouteProps> = (props) => {
  const {path, children, isLogin} = props;
  console.log("private",isLogin, path);
  return (
    <Route
      path={path}
      render={({ location }) =>
        isLogin ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/redirect",
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}

export default PrivateRoute;