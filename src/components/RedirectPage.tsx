import React from 'react';
import { Typography } from '@material-ui/core';
import { useHistory, useLocation } from 'react-router-dom';

interface RedirectPageProps {
  onLoginButtonClick: () => void;
}

const RedirectPage: React.FC<RedirectPageProps> = (props) => {
  const history = useHistory();
  const location = useLocation();

  return <div>
    {console.log(history, location)}
    <Typography>Please login to see content</Typography>
  </div>
}

export default RedirectPage;