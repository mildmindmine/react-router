import React, { Suspense, lazy, useState } from 'react';
import AppBar from '@material-ui/core/AppBar'
import { Toolbar, Typography, makeStyles, Button } from '@material-ui/core';
import { Route, Switch, Link, NavLink, useHistory, useLocation } from 'react-router-dom';

const Pokemons = lazy(() => import('./components/Pokemon'));
const PokemonInfo = lazy(() => import('./components/PokemonInfo'));
const NotFound = lazy(() => import('./components/NotFound'));
const PreventNavigation = lazy(() => import('./components/PreventNavigation'));
const RedirectPage = lazy(() => import('./components/RedirectPage'));
const PrivateRoute = lazy(() => import('./components/PrivateRoute'));

// import PokemonInfo from './components/PokemonInfo'

const useStyles = makeStyles(theme => ({
  root: {
    flex: 1,
    justifyContent: 'space-between'
  },
  menucontainer: {
    display: 'flex',
    flexBasis: 1,
    flex: 1,
    justifyContent: 'flex-end',
  },
  navLink: {
    color: '#eee',
    textDecoration: 'none',
    marginLeft: theme.spacing(4),
    padding: theme.spacing(2),
  },
  activeNavLink: {
    color: 'black',
    backgroundColor: 'pink',
    textDecoration: 'none',
    fontSize: 20,
    marginLeft: theme.spacing(4),
    padding: theme.spacing(2),
    '& p ': {
      fontSize: 20
    },
  },
  navButton: {
    marginLeft: theme.spacing(4),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    color: 'black'
  }
}))

const App: React.FC = () => {
  const classes = useStyles();
  const [isLogin, setIsLogin] = useState<boolean>(false);
  const history = useHistory();
  const {pathname} = useLocation();

  const onLoginAndLogoutButtonClick = () => {
    setIsLogin(!isLogin);
    if(pathname === "/redirect") {
      history.replace("/loggedin");
    }
  }

  return (
    <div>
      <AppBar position="static" >
        <Toolbar className={classes.root}>
          <Link to='/' className={classes.navLink}><Typography>Hello</Typography></Link>
          <Button 
            variant="contained" 
            color="inherit"
            className={classes.navButton}
            onClick={onLoginAndLogoutButtonClick}
          >
            {isLogin? "LOG OUT": "LOG IN"}
          </Button>
          <div className={classes.menucontainer}>
            {/* 
            <Link to="/about" className={classes.navLink}><Typography>Prevent Navigation</Typography></Link>
            <Link to="/pokemons" className={classes.navLink}><Typography>Pokemons</Typography></Link> 
            <Link to="/redirect" className={classes.navLink}><Typography>Redirect</Typography></Link>
            */}
            <NavLink to="/about" activeClassName={classes.activeNavLink} className={classes.navLink}><Typography>Prevent Navigation</Typography></NavLink>
            <NavLink to="/pokemons" activeClassName={classes.activeNavLink} className={classes.navLink}><Typography>Pokemons</Typography></NavLink>
            <NavLink to="/loggedin" activeClassName={classes.activeNavLink} className={classes.navLink}><Typography>Redirect</Typography></NavLink>
          </div>
        </Toolbar>
      </AppBar>
      <Suspense fallback={<div>Loading...</div>}>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/pokemons" component={Pokemons}/>
          <Route path="/about" component={PreventNavigation}/>
          <Route path="/pokemons/:id" component={PokemonInfo}/>
          <PrivateRoute path="/loggedin" isLogin={isLogin}>
            <LoggedIn/>
          </PrivateRoute>
          <Route path="/redirect" component={RedirectPage}/>
          <Route component={NotFound} />
        </Switch>
        {/* <Route path="/" component={Home}/>
        <Route path="/about" component={About}/> */}
        {/* <Route exact path="/">
          <Home />
        </Route> */}
      </Suspense>
    </div>
  );
}

const Home = () => <div><h1>Home</h1></div>;
const LoggedIn = () => <div><h1>YAYYY You've Logged In</h1></div>;


export default App;
